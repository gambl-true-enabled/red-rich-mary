package com.red.rich.mary

import android.content.Context
import android.net.ConnectivityManager
import android.webkit.JavascriptInterface

private const val RICH_TABLE = "com.RICH.table.123"
private const val RICH_ARGS = "com.RICH.value.456"

class JavaScript(
    private val callback: Callback
) {

    interface Callback {
        fun needAuth()
        fun authorized()
    }

    @JavascriptInterface
    fun onAuthorized() {
        callback.authorized()
    }

    @JavascriptInterface
    fun onNeedAuth() {
        callback.needAuth()
    }
}

fun Context.saveLink(deepArgs: String) {
    val sharedPreferences = getSharedPreferences(RICH_TABLE, Context.MODE_PRIVATE)
    sharedPreferences.edit().putString(RICH_ARGS, deepArgs).apply()
}

fun Context.getArgs(): String? {
    val sharedPreferences = getSharedPreferences(RICH_TABLE, Context.MODE_PRIVATE)
    return sharedPreferences.getString(RICH_ARGS, null)
}

fun Context.isNetworkConnected(): Boolean {
    val cm = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val netInfo = cm.activeNetworkInfo
    return netInfo != null && netInfo.isConnected
}
